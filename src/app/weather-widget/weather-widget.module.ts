import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WeatherWidgetComponent} from './weather-widget.component';
import {CityWeatherComponent} from './components/city-weather/city-weather.component';
import {WeatherIconComponent} from './components/weather-icon/weather-icon.component';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {SearchCityComponent} from './components/search-city/search-city.component';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzSpinModule} from 'ng-zorro-antd/spin';
import {NzAlertModule} from 'ng-zorro-antd/alert';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {FormsModule} from '@angular/forms';
import {FlagIconComponent} from './components/flag-icon/flag-icon.component';
import {ForecastComponent} from './components/forecast/forecast.component';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {TemperatureUnitPipe} from './temperature-unit.pipe';
import {SpeedUnitPipe} from './speed-unit.pipe';
import {UnitSelectorComponent} from './components/unit-selector/unit-selector.component';
import {WeatherWidgetConfig} from './common/weather-widget-config';
import {WEATHER_WIDGET_CONFIG_TOKEN} from './common/weather-widget-config.token';

@NgModule({
	declarations: [
		WeatherWidgetComponent,
		CityWeatherComponent,
		WeatherIconComponent,
		SearchCityComponent,
		FlagIconComponent,
		ForecastComponent,
		TemperatureUnitPipe,
		SpeedUnitPipe,
		UnitSelectorComponent,
	],
	imports: [
		CommonModule,
		NzIconModule,
		NzToolTipModule,
		NzSelectModule,
		NzSpinModule,
		NzAlertModule,
		NzButtonModule,
		FormsModule,
		NzRadioModule,
		NzDropDownModule,
	],
	exports: [
		WeatherWidgetComponent,
		CityWeatherComponent,
		WeatherIconComponent,
		SearchCityComponent,
		FlagIconComponent,
		ForecastComponent,
		TemperatureUnitPipe,
		SpeedUnitPipe,
	],
})
export class WeatherWidgetModule {
	public static forRoot(config: WeatherWidgetConfig): ModuleWithProviders<WeatherWidgetModule> {
		return {
			ngModule: WeatherWidgetModule,
			providers: [{provide: WEATHER_WIDGET_CONFIG_TOKEN, useValue: config}],
		};
	}
}
