import {Pipe, PipeTransform} from '@angular/core';
import {WeatherUnit} from './common/weather-unit';

@Pipe({
	name: 'tempUnit',
})
export class TemperatureUnitPipe implements PipeTransform {
	public transform(value: number, unit: WeatherUnit): string {
		return `${Math.round(value)} ${this.unitName(unit)}`;
	}

	private unitName(unit: WeatherUnit): string {
		switch (unit) {
			case WeatherUnit.IMPERIAL:
				return '°F';
			case WeatherUnit.METRIC:
				return '°C';
			default:
				return '°K';
		}
	}
}
