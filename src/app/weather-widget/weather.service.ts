import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CitySearch} from './common/city-search';
import {CityWeather} from './common/city-weather';
import {Forecast} from './common/forecast';
import {WeatherUnit} from './common/weather-unit';
import {WEATHER_WIDGET_CONFIG_TOKEN} from './common/weather-widget-config.token';
import {WeatherWidgetConfig} from './common/weather-widget-config';

@Injectable({
	providedIn: 'root',
})
export class WeatherService {
	constructor(
		private httpClient: HttpClient,
		@Inject(WEATHER_WIDGET_CONFIG_TOKEN) private config: WeatherWidgetConfig,
	) {}

	public find(cityName: string): Observable<CitySearch> {
		return this.httpClient.get<CitySearch>(`${this.config.apiUrl}/find`, {
			params: {q: cityName, appId: this.config.appId},
		});
	}

	public weather(cityId: number, units: WeatherUnit = WeatherUnit.STANDARD): Observable<CityWeather> {
		return this.httpClient.get<CityWeather>(`${this.config.apiUrl}/weather`, {
			params: {id: cityId, units, appId: this.config.appId},
		});
	}

	public forecast(cityId: number, units: WeatherUnit = WeatherUnit.STANDARD): Observable<Forecast> {
		return this.httpClient.get<Forecast>(`${this.config.apiUrl}/forecast`, {
			params: {id: cityId, units, appId: this.config.appId},
		});
	}
}
