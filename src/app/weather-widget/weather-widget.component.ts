import {ChangeDetectionStrategy, Component} from '@angular/core';
import {StoreService} from './store.service';
import {animate, style, transition, trigger} from '@angular/animations';
import {WeatherUnit} from './common/weather-unit';
import {preventInitialChildAnimations} from './common/prevent-initial-animation';

const WEATHER_WIDGET_KEY: string = 'weather-widget';
export const DEFAULT_CITIES: number[] = [
	/* Amsterdam */ 2759794 /* Moscow */, 524901 /* London */, 2643743 /* Tallinn */, 588409 /* New York City */,
	5128581,
];

@Component({
	animations: [
		trigger('inOutAnimation', [
			transition(':enter', [
				style({height: 0, opacity: 0, transform: 'translateX(-50%)'}),
				animate('.2s ease-out', style({height: '*', opacity: 1, transform: 'translateX(0)'})),
			]),
			transition(':leave', [
				style({height: '*', opacity: 1, transform: 'translateX(0)'}),
				animate('.2s ease-in', style({height: 0, opacity: 0, transform: 'translateX(50%)'})),
			]),
		]),
		preventInitialChildAnimations,
	],
	selector: 'weather-widget',
	templateUrl: './weather-widget.component.html',
	styleUrls: ['./weather-widget.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeatherWidgetComponent {
	public cityIds: Set<number> = new Set<number>();
	public unit: WeatherUnit = WeatherUnit.METRIC;
	public expandedCityId: number | null = null;

	constructor(private storeService: StoreService) {
		/** loading the saved list of cities, if there are no saves, then we take the default list */
		this.cityIds = new Set(
			this.storeService.get(WEATHER_WIDGET_KEY, (v: string | null) => v?.split(',').map(Number)) ??
				DEFAULT_CITIES,
		);
	}

	public remove(id: number): void {
		/*
			Remove the city from the view, update the store,
		   and clear the current expanded city if it was the city being removed,
		   because if the user adds it again, it has to be collapsed
		*/
		this.cityIds.delete(id);
		this.expandedCityId = id === this.expandedCityId ? null : this.expandedCityId;
		this.updateStore();
	}

	public add(id: number): void {
		/* Add new city to the view and update the store */
		this.cityIds.add(id);
		this.updateStore();
	}

	private updateStore(): void {
		/* Just update the store by the current city list */
		this.storeService.set(WEATHER_WIDGET_KEY, this.cityIds, (cityIds: Set<number>) =>
			Array.from(cityIds).join(','),
		);
	}
}
