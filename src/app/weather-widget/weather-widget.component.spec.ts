import {HarnessLoader, TestElement} from '@angular/cdk/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
import {Component} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {provideWeatherMockService} from './testing/weather.mock.service';
import {WeatherWidgetModule} from './weather-widget.module';
import {WeatherWidgetHarness} from './testing/weather-widget.harness';
import {SearchCityHarness} from './components/search-city/testing/search-city.harness';
import {FIND_MOCK_DATA} from './testing/mocks/find.mock';
import {CityWeatherHarness} from './components/city-weather/testing/city-weather.harness';
import {DEFAULT_CITIES} from './weather-widget.component';
import {provideFakeStoreService} from './testing/fake-store.service';

describe('WeatherWidgetComponent', () => {
	let fixture: ComponentFixture<TestingComponent>;
	let loader: HarnessLoader;
	let component: WeatherWidgetHarness;
	let searchCity: SearchCityHarness;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [WeatherWidgetModule, NoopAnimationsModule, HttpClientModule],
			providers: [provideWeatherMockService(), provideFakeStoreService()],
			declarations: [TestingComponent],
		}).compileComponents();
		fixture = TestBed.createComponent(TestingComponent);
		loader = TestbedHarnessEnvironment.loader(fixture);
		component = await loader.getHarness(WeatherWidgetHarness);
		searchCity = await loader.getHarness(SearchCityHarness);
	});

	it('should render all cities', async () => {
		expect((await component.getCityWeathers()).length).toBe(DEFAULT_CITIES.length);
	});

	it('should add new city', async () => {
		await searchCity.enterText(FIND_MOCK_DATA.list[0].name);

		const option: TestElement | null = await searchCity.getOption(FIND_MOCK_DATA.list[0].name);

		await option?.click();

		expect((await component.getCityWeathers()).length).toBe(DEFAULT_CITIES.length + 1);
	});

	it('should remove city by remove button', async () => {
		const city: CityWeatherHarness | undefined = (await component.getCityWeathers())[0];

		await city?.clickByDeleteButton();

		expect((await component.getCityWeathers()).length).toBe(DEFAULT_CITIES.length - 1);
	});
});

@Component({
	selector: 'testing-component',
	template: ` <weather-widget></weather-widget> `,
})
export class TestingComponent {}
