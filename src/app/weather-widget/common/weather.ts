export interface Weather {
	/* Weather condition id */
	id: number;
	/* Weather condition name */
	main: string;
	/* Weather condition description */
	description: string;
	/* Weather condition icon */
	icon: string;
}
