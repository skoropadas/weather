export interface Wind {
	/* Wind speed */
	speed: number;
	/* Wind direction */
	deg: number;
	gust?: unknown;
}
