export enum WeatherUnit {
	STANDARD = 'standard',
	METRIC = 'metric',
	IMPERIAL = 'imperial',
}
