export interface Sys {
	id?: number;
	country: string;
	type?: number;
	sunrise?: number;
	sunset?: number;
}
