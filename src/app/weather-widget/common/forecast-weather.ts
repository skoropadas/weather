import {Temperature} from './temperature';
import {Weather} from './weather';
import {Wind} from './wind';
import {Clouds} from './clouds';

export interface ForecastWeather {
	dt: number;
	dt_txt: string;
	main: Temperature;
	weather: Weather[];
	clouds: Clouds;
	wind: Wind;
	visibility: number;
	pop: number;
	rain?: unknown;
	sys?: unknown;
}
