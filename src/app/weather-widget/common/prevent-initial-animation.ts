import {AnimationTriggerMetadata, transition, trigger} from '@angular/animations';

/* Trigger that helps to prevent child's initial animation */
export const preventInitialChildAnimations: AnimationTriggerMetadata = trigger('preventInitialChildAnimations', [
	transition(':enter', []),
]);
