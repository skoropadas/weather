import {CityWeather} from './city-weather';

export interface CitySearch {
	count: number;
	message: string;
	cod: string;
	list: CityWeather[];
}
