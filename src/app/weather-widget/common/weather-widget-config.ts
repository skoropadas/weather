export interface WeatherWidgetConfig {
	appId: string;
	apiUrl: string;
}
