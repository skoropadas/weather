export interface Temperature {
	/* Temperature */
	temp: number;
	/* Temperature. This temperature parameter accounts for the human perception of weather */
	feels_like: number;
	/* Atmospheric pressure */
	pressure: number;
	/* Humidity, % */
	humidity: number;
	/* Minimum temperature at the moment */
	temp_min: number;
	/* Maximum temperature at the moment */
	temp_max: number;
	/* Atmospheric pressure on the sea level, hPa */
	sea_level?: number;
	/* Atmospheric pressure on the ground level, hPa */
	grnd_level?: number;
	temp_kf?: number;
}
