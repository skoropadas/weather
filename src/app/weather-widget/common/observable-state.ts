/* Interfaces that help determine the current state of the Observable */

export type ObservableState<R, E> = ObservablePending | ObservableError<E> | ObservableResult<R>;

export class ObservablePending {
	public readonly state: 'pending' = 'pending';
}

export class ObservableError<T> {
	public readonly state: 'error' = 'error';

	constructor(public readonly error: T) {}
}

export class ObservableResult<T> {
	public readonly state: 'result' = 'result';

	constructor(public readonly data: T) {}
}
