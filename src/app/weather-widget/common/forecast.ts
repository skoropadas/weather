import {ForecastWeather} from './forecast-weather';

export interface Forecast {
	cod: string;
	message: number;
	cnt: number;
	list: ForecastWeather[];
	city: unknown;
}
