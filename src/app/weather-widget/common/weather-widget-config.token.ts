import {InjectionToken} from '@angular/core';
import {WeatherWidgetConfig} from './weather-widget-config';

export const WEATHER_WIDGET_CONFIG_TOKEN: InjectionToken<WeatherWidgetConfig> = new InjectionToken<WeatherWidgetConfig>(
	'WEATHER_WIDGET_CONFIG_TOKEN',
);
