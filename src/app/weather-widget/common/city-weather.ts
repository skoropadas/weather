import {Coordinates} from './coordinates';
import {Temperature} from './temperature';
import {Sys} from './sys';
import {Weather} from './weather';
import {Wind} from './wind';
import {Clouds} from './clouds';

export interface CityWeather {
	id: number;
	name: string;
	dt: number;
	base?: string;
	visibility?: number;
	clouds?: Clouds;
	timezone?: number;
	cod?: number;
	main: Temperature;
	coord: Coordinates;
	sys: Sys;
	weather: Weather[];
	wind: Wind;
	rain?: unknown;
	snow?: unknown;
}
