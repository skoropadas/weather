import {Injectable} from '@angular/core';
import {isString} from './common/is-string';

const DEFAULT_SERIALIZE: (v: unknown) => string = (v: unknown) => String(v);

@Injectable({
	providedIn: 'root',
})
export class StoreService {
	public set(key: string, data: string): void;
	public set<T>(key: string, data: T, serialize: (v: T) => string): void;
	public set<T>(key: string, data: T | string, serialize: (v: T) => string = DEFAULT_SERIALIZE): void {
		return localStorage.setItem(key, !isString(data) ? serialize(data) : data);
	}

	public get(key: string): string | null;
	public get<T>(key: string, deserialize: (v: string | null) => T): T;
	public get<T>(key: string, deserialize?: (v: string | null) => T): T | string | null {
		return deserialize ? deserialize(localStorage.getItem(key)) : localStorage.getItem(key);
	}
}
