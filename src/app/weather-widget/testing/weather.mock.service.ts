import {Provider} from '@angular/core';
import {FIND_MOCK_DATA} from './mocks/find.mock';
import {WeatherService} from '../weather.service';
import {WEATHER_MOCK_DATA} from './mocks/weather.mock';
import {FORECAST_MOCK_DATA} from './mocks/forecast.mock';
import {of} from 'rxjs';

/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */

/* eslint-disable @typescript-eslint/no-unsafe-member-access */

export function provideWeatherMockService(): Provider {
	const weatherMockService: any = jasmine.createSpyObj(['find', 'weather', 'forecast']);

	weatherMockService.find.and.returnValue(of(FIND_MOCK_DATA));
	weatherMockService.weather.and.callFake((cityId: number) => of(WEATHER_MOCK_DATA.get(cityId)));
	weatherMockService.forecast.and.returnValue(of(FORECAST_MOCK_DATA));

	return {provide: WeatherService, useValue: weatherMockService};
}
