import {Injectable, Provider} from '@angular/core';
import {StoreService} from '../store.service';

export function provideFakeStoreService(): Provider {
	return {provide: StoreService, useClass: FakeStoreService};
}

@Injectable()
export class FakeStoreService extends StoreService {
	public override set(): void {}

	public override get(): null {
		return null;
	}
}
