import {BaseHarnessFilters, ComponentHarness, HarnessPredicate} from '@angular/cdk/testing';
import {CityWeatherHarness} from '../components/city-weather/testing/city-weather.harness';

export class WeatherWidgetHarness extends ComponentHarness {
	public static hostSelector: string = 'weather-widget';

	public static with(options: BaseHarnessFilters): HarnessPredicate<WeatherWidgetHarness> {
		return new HarnessPredicate<WeatherWidgetHarness>(WeatherWidgetHarness, options);
	}

	public async getCityWeathers(): Promise<CityWeatherHarness[]> {
		return await this.locatorForAll(CityWeatherHarness)();
	}
}
