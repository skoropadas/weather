import {CityWeather} from '../../common/city-weather';

export const WEATHER_MOCK_DATA: Map<number, CityWeather> = new Map<number, CityWeather>([
	[
		2759794,
		{
			coord: {
				lon: 4.8897,
				lat: 52.374,
			},
			weather: [
				{
					id: 801,
					main: 'Clouds',
					description: 'few clouds',
					icon: '02n',
				},
			],
			base: 'stations',
			main: {
				temp: 4.64,
				feels_like: 0.93,
				temp_min: 3.47,
				temp_max: 5.5,
				pressure: 997,
				humidity: 80,
			},
			visibility: 10000,
			wind: {
				speed: 4.92,
				deg: 141,
				gust: 9.83,
			},
			clouds: {
				all: 20,
			},
			dt: 1638904564,
			sys: {
				type: 2,
				id: 2011826,
				country: 'NL',
				sunrise: 1638862566,
				sunset: 1638890886,
			},
			timezone: 3600,
			id: 2759794,
			name: 'Amsterdam',
			cod: 200,
		},
	],
	[
		524901,
		{
			coord: {
				lon: 37.6156,
				lat: 55.7522,
			},
			weather: [
				{
					id: 600,
					main: 'Snow',
					description: 'light snow',
					icon: '13n',
				},
			],
			base: 'stations',
			main: {
				temp: -0.91,
				feels_like: -4.58,
				temp_min: -2.07,
				temp_max: 0.13,
				pressure: 1015,
				humidity: 92,
				sea_level: 1015,
				grnd_level: 997,
			},
			visibility: 10000,
			wind: {
				speed: 2.98,
				deg: 325,
				gust: 5.92,
			},
			snow: {
				'1h': 0.39,
			},
			clouds: {
				all: 100,
			},
			dt: 1638917859,
			sys: {
				type: 2,
				id: 2018597,
				country: 'RU',
				sunrise: 1638942326,
				sunset: 1638968264,
			},
			timezone: 10800,
			id: 524901,
			name: 'Moscow',
			cod: 200,
		},
	],
	[
		2643743,
		{
			coord: {
				lon: -0.1257,
				lat: 51.5085,
			},
			weather: [
				{
					id: 803,
					main: 'Clouds',
					description: 'broken clouds',
					icon: '04n',
				},
			],
			base: 'stations',
			main: {
				temp: 5.46,
				feels_like: 5.46,
				temp_min: 4.44,
				temp_max: 6.82,
				pressure: 985,
				humidity: 85,
			},
			visibility: 10000,
			wind: {
				speed: 0.45,
				deg: 96,
				gust: 1.34,
			},
			clouds: {
				all: 68,
			},
			dt: 1638917911,
			sys: {
				type: 2,
				id: 2019646,
				country: 'GB',
				sunrise: 1638863514,
				sunset: 1638892346,
			},
			timezone: 0,
			id: 2643743,
			name: 'London',
			cod: 200,
		},
	],
	[
		588409,
		{
			coord: {
				lon: 24.7535,
				lat: 59.437,
			},
			weather: [
				{
					id: 600,
					main: 'Snow',
					description: 'light snow',
					icon: '13n',
				},
			],
			base: 'stations',
			main: {
				temp: -8.79,
				feels_like: -13.53,
				temp_min: -10.82,
				temp_max: -8.64,
				pressure: 1017,
				humidity: 83,
			},
			visibility: 10000,
			wind: {
				speed: 2.57,
				deg: 200,
			},
			clouds: {
				all: 90,
			},
			dt: 1638918093,
			sys: {
				type: 1,
				id: 1330,
				country: 'EE',
				sunrise: 1638947037,
				sunset: 1638969729,
			},
			timezone: 7200,
			id: 588409,
			name: 'Tallinn',
			cod: 200,
		},
	],
	[
		5128581,
		{
			coord: {
				lon: -74.006,
				lat: 40.7143,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			base: 'stations',
			main: {
				temp: 3.39,
				feels_like: 1.24,
				temp_min: 0.98,
				temp_max: 4.99,
				pressure: 1022,
				humidity: 52,
			},
			visibility: 10000,
			wind: {
				speed: 2.24,
				deg: 258,
				gust: 3.58,
			},
			clouds: {
				all: 98,
			},
			dt: 1638917933,
			sys: {
				type: 2,
				id: 2008776,
				country: 'US',
				sunrise: 1638878814,
				sunset: 1638912520,
			},
			timezone: -18000,
			id: 5128581,
			name: 'New York',
			cod: 200,
		},
	],
]);
