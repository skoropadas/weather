import {Forecast} from '../../common/forecast';

export const FORECAST_MOCK_DATA: Forecast = {
	cod: '200',
	message: 0,
	cnt: 40,
	list: [
		{
			dt: 1638910800,
			main: {
				temp: 4.47,
				feels_like: -0.88,
				temp_min: 4.2,
				temp_max: 4.47,
				pressure: 996,
				sea_level: 996,
				grnd_level: 992,
				humidity: 83,
				temp_kf: 0.27,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 47,
			},
			wind: {
				speed: 9.17,
				deg: 151,
				gust: 17.98,
			},
			visibility: 7341,
			pop: 0.99,
			rain: {
				'3h': 1.59,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-07 21:00:00',
		},
		{
			dt: 1638921600,
			main: {
				temp: 4.62,
				feels_like: -0.15,
				temp_min: 4.62,
				temp_max: 4.63,
				pressure: 993,
				sea_level: 993,
				grnd_level: 990,
				humidity: 90,
				temp_kf: -0.01,
			},
			weather: [
				{
					id: 501,
					main: 'Rain',
					description: 'moderate rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 73,
			},
			wind: {
				speed: 7.49,
				deg: 165,
				gust: 14.91,
			},
			visibility: 8389,
			pop: 1,
			rain: {
				'3h': 4.86,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-08 00:00:00',
		},
		{
			dt: 1638932400,
			main: {
				temp: 5.48,
				feels_like: 1.71,
				temp_min: 5.48,
				temp_max: 5.48,
				pressure: 992,
				sea_level: 992,
				grnd_level: 991,
				humidity: 86,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 5.51,
				deg: 195,
				gust: 12.23,
			},
			visibility: 10000,
			pop: 0.75,
			rain: {
				'3h': 0.3,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-08 03:00:00',
		},
		{
			dt: 1638943200,
			main: {
				temp: 5.58,
				feels_like: 1.2,
				temp_min: 5.58,
				temp_max: 5.58,
				pressure: 993,
				sea_level: 993,
				grnd_level: 992,
				humidity: 80,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 94,
			},
			wind: {
				speed: 7.17,
				deg: 204,
				gust: 12.5,
			},
			visibility: 10000,
			pop: 0.55,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-08 06:00:00',
		},
		{
			dt: 1638954000,
			main: {
				temp: 4.78,
				feels_like: 0.51,
				temp_min: 4.78,
				temp_max: 4.78,
				pressure: 995,
				sea_level: 995,
				grnd_level: 993,
				humidity: 84,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04d',
				},
			],
			clouds: {
				all: 91,
			},
			wind: {
				speed: 6.26,
				deg: 193,
				gust: 12.51,
			},
			visibility: 10000,
			pop: 0.16,
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-08 09:00:00',
		},
		{
			dt: 1638964800,
			main: {
				temp: 5.68,
				feels_like: 1.8,
				temp_min: 5.68,
				temp_max: 5.68,
				pressure: 996,
				sea_level: 996,
				grnd_level: 995,
				humidity: 81,
				temp_kf: 0,
			},
			weather: [
				{
					id: 803,
					main: 'Clouds',
					description: 'broken clouds',
					icon: '04d',
				},
			],
			clouds: {
				all: 80,
			},
			wind: {
				speed: 5.89,
				deg: 185,
				gust: 11.04,
			},
			visibility: 10000,
			pop: 0.12,
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-08 12:00:00',
		},
		{
			dt: 1638975600,
			main: {
				temp: 5.55,
				feels_like: 1.69,
				temp_min: 5.55,
				temp_max: 5.55,
				pressure: 998,
				sea_level: 998,
				grnd_level: 996,
				humidity: 81,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04d',
				},
			],
			clouds: {
				all: 90,
			},
			wind: {
				speed: 5.76,
				deg: 188,
				gust: 10.62,
			},
			visibility: 10000,
			pop: 0.05,
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-08 15:00:00',
		},
		{
			dt: 1638986400,
			main: {
				temp: 4.37,
				feels_like: 0.48,
				temp_min: 4.37,
				temp_max: 4.37,
				pressure: 999,
				sea_level: 999,
				grnd_level: 997,
				humidity: 87,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 93,
			},
			wind: {
				speed: 5.15,
				deg: 183,
				gust: 11.77,
			},
			visibility: 10000,
			pop: 0.05,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-08 18:00:00',
		},
		{
			dt: 1638997200,
			main: {
				temp: 3.65,
				feels_like: -0.33,
				temp_min: 3.65,
				temp_max: 3.65,
				pressure: 1000,
				sea_level: 1000,
				grnd_level: 999,
				humidity: 90,
				temp_kf: 0,
			},
			weather: [
				{
					id: 803,
					main: 'Clouds',
					description: 'broken clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 75,
			},
			wind: {
				speed: 4.95,
				deg: 185,
				gust: 11.2,
			},
			visibility: 10000,
			pop: 0.12,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-08 21:00:00',
		},
		{
			dt: 1639008000,
			main: {
				temp: 2.91,
				feels_like: -1.18,
				temp_min: 2.91,
				temp_max: 2.91,
				pressure: 1000,
				sea_level: 1000,
				grnd_level: 999,
				humidity: 92,
				temp_kf: 0,
			},
			weather: [
				{
					id: 803,
					main: 'Clouds',
					description: 'broken clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 58,
			},
			wind: {
				speed: 4.82,
				deg: 178,
				gust: 11.04,
			},
			visibility: 10000,
			pop: 0.03,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-09 00:00:00',
		},
		{
			dt: 1639018800,
			main: {
				temp: 3.24,
				feels_like: -1.05,
				temp_min: 3.24,
				temp_max: 3.24,
				pressure: 1000,
				sea_level: 1000,
				grnd_level: 999,
				humidity: 88,
				temp_kf: 0,
			},
			weather: [
				{
					id: 803,
					main: 'Clouds',
					description: 'broken clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 70,
			},
			wind: {
				speed: 5.37,
				deg: 180,
				gust: 11.28,
			},
			visibility: 10000,
			pop: 0.12,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-09 03:00:00',
		},
		{
			dt: 1639029600,
			main: {
				temp: 3.81,
				feels_like: -0.65,
				temp_min: 3.81,
				temp_max: 3.81,
				pressure: 1000,
				sea_level: 1000,
				grnd_level: 999,
				humidity: 92,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 85,
			},
			wind: {
				speed: 6.08,
				deg: 184,
				gust: 10.8,
			},
			visibility: 10000,
			pop: 0.32,
			rain: {
				'3h': 0.2,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-09 06:00:00',
		},
		{
			dt: 1639040400,
			main: {
				temp: 5.03,
				feels_like: 1.22,
				temp_min: 5.03,
				temp_max: 5.03,
				pressure: 1000,
				sea_level: 1000,
				grnd_level: 999,
				humidity: 87,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10d',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 5.34,
				deg: 192,
				gust: 10.48,
			},
			visibility: 10000,
			pop: 0.49,
			rain: {
				'3h': 0.32,
			},
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-09 09:00:00',
		},
		{
			dt: 1639051200,
			main: {
				temp: 5.89,
				feels_like: 2.4,
				temp_min: 5.89,
				temp_max: 5.89,
				pressure: 1000,
				sea_level: 1000,
				grnd_level: 998,
				humidity: 86,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10d',
				},
			],
			clouds: {
				all: 99,
			},
			wind: {
				speed: 5.1,
				deg: 211,
				gust: 9.05,
			},
			visibility: 10000,
			pop: 0.47,
			rain: {
				'3h': 0.31,
			},
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-09 12:00:00',
		},
		{
			dt: 1639062000,
			main: {
				temp: 6.34,
				feels_like: 3.36,
				temp_min: 6.34,
				temp_max: 6.34,
				pressure: 1000,
				sea_level: 1000,
				grnd_level: 999,
				humidity: 82,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10d',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 4.27,
				deg: 227,
				gust: 9.19,
			},
			visibility: 10000,
			pop: 0.76,
			rain: {
				'3h': 0.79,
			},
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-09 15:00:00',
		},
		{
			dt: 1639072800,
			main: {
				temp: 5.9,
				feels_like: 2.82,
				temp_min: 5.9,
				temp_max: 5.9,
				pressure: 1001,
				sea_level: 1001,
				grnd_level: 1000,
				humidity: 84,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 99,
			},
			wind: {
				speed: 4.26,
				deg: 212,
				gust: 9.06,
			},
			visibility: 10000,
			pop: 0.74,
			rain: {
				'3h': 0.77,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-09 18:00:00',
		},
		{
			dt: 1639083600,
			main: {
				temp: 5.63,
				feels_like: 2.23,
				temp_min: 5.63,
				temp_max: 5.63,
				pressure: 1000,
				sea_level: 1000,
				grnd_level: 999,
				humidity: 81,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 94,
			},
			wind: {
				speed: 4.78,
				deg: 200,
				gust: 9.78,
			},
			visibility: 10000,
			pop: 0.32,
			rain: {
				'3h': 0.19,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-09 21:00:00',
		},
		{
			dt: 1639094400,
			main: {
				temp: 3.4,
				feels_like: -0.4,
				temp_min: 3.4,
				temp_max: 3.4,
				pressure: 999,
				sea_level: 999,
				grnd_level: 998,
				humidity: 93,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 88,
			},
			wind: {
				speed: 4.51,
				deg: 181,
				gust: 10.25,
			},
			visibility: 10000,
			pop: 0.18,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-10 00:00:00',
		},
		{
			dt: 1639105200,
			main: {
				temp: 3.06,
				feels_like: -0.99,
				temp_min: 3.06,
				temp_max: 3.06,
				pressure: 996,
				sea_level: 996,
				grnd_level: 995,
				humidity: 89,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 91,
			},
			wind: {
				speed: 4.81,
				deg: 162,
				gust: 10.78,
			},
			visibility: 10000,
			pop: 0.05,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-10 03:00:00',
		},
		{
			dt: 1639116000,
			main: {
				temp: 2.68,
				feels_like: -1.67,
				temp_min: 2.68,
				temp_max: 2.68,
				pressure: 994,
				sea_level: 994,
				grnd_level: 992,
				humidity: 89,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 96,
			},
			wind: {
				speed: 5.2,
				deg: 149,
				gust: 11.61,
			},
			visibility: 10000,
			pop: 0.01,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-10 06:00:00',
		},
		{
			dt: 1639126800,
			main: {
				temp: 2.12,
				feels_like: -2.37,
				temp_min: 2.12,
				temp_max: 2.12,
				pressure: 993,
				sea_level: 993,
				grnd_level: 992,
				humidity: 96,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04d',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 5.19,
				deg: 117,
				gust: 11.07,
			},
			visibility: 1338,
			pop: 0.12,
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-10 09:00:00',
		},
		{
			dt: 1639137600,
			main: {
				temp: 3.29,
				feels_like: -0.86,
				temp_min: 3.29,
				temp_max: 3.29,
				pressure: 993,
				sea_level: 993,
				grnd_level: 991,
				humidity: 85,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04d',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 5.11,
				deg: 106,
				gust: 9.33,
			},
			visibility: 10000,
			pop: 0.2,
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-10 12:00:00',
		},
		{
			dt: 1639148400,
			main: {
				temp: 3.16,
				feels_like: -0.16,
				temp_min: 3.16,
				temp_max: 3.16,
				pressure: 995,
				sea_level: 995,
				grnd_level: 994,
				humidity: 86,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10d',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 3.62,
				deg: 70,
				gust: 6.87,
			},
			visibility: 10000,
			pop: 0.4,
			rain: {
				'3h': 0.15,
			},
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-10 15:00:00',
		},
		{
			dt: 1639159200,
			main: {
				temp: 3.43,
				feels_like: 0.45,
				temp_min: 3.43,
				temp_max: 3.43,
				pressure: 998,
				sea_level: 998,
				grnd_level: 997,
				humidity: 84,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 3.22,
				deg: 35,
				gust: 6.54,
			},
			visibility: 10000,
			pop: 0.4,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-10 18:00:00',
		},
		{
			dt: 1639170000,
			main: {
				temp: 3.12,
				feels_like: 0.59,
				temp_min: 3.12,
				temp_max: 3.12,
				pressure: 1002,
				sea_level: 1002,
				grnd_level: 1001,
				humidity: 86,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 2.59,
				deg: 19,
				gust: 5.8,
			},
			visibility: 10000,
			pop: 0.05,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-10 21:00:00',
		},
		{
			dt: 1639180800,
			main: {
				temp: 2.06,
				feels_like: 0.37,
				temp_min: 2.06,
				temp_max: 2.06,
				pressure: 1005,
				sea_level: 1005,
				grnd_level: 1004,
				humidity: 89,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 98,
			},
			wind: {
				speed: 1.66,
				deg: 348,
				gust: 2.09,
			},
			visibility: 10000,
			pop: 0,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-11 00:00:00',
		},
		{
			dt: 1639191600,
			main: {
				temp: 1.99,
				feels_like: 1.99,
				temp_min: 1.99,
				temp_max: 1.99,
				pressure: 1008,
				sea_level: 1008,
				grnd_level: 1007,
				humidity: 88,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 0.93,
				deg: 341,
				gust: 1.11,
			},
			visibility: 10000,
			pop: 0,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-11 03:00:00',
		},
		{
			dt: 1639202400,
			main: {
				temp: 2.51,
				feels_like: 1.02,
				temp_min: 2.51,
				temp_max: 2.51,
				pressure: 1011,
				sea_level: 1011,
				grnd_level: 1010,
				humidity: 87,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 97,
			},
			wind: {
				speed: 1.56,
				deg: 263,
				gust: 1.72,
			},
			visibility: 10000,
			pop: 0.05,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-11 06:00:00',
		},
		{
			dt: 1639213200,
			main: {
				temp: 4.6,
				feels_like: 3.31,
				temp_min: 4.6,
				temp_max: 4.6,
				pressure: 1014,
				sea_level: 1014,
				grnd_level: 1013,
				humidity: 88,
				temp_kf: 0,
			},
			weather: [
				{
					id: 803,
					main: 'Clouds',
					description: 'broken clouds',
					icon: '04d',
				},
			],
			clouds: {
				all: 74,
			},
			wind: {
				speed: 1.64,
				deg: 269,
				gust: 3.9,
			},
			visibility: 10000,
			pop: 0.34,
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-11 09:00:00',
		},
		{
			dt: 1639224000,
			main: {
				temp: 7.71,
				feels_like: 5.36,
				temp_min: 7.71,
				temp_max: 7.71,
				pressure: 1016,
				sea_level: 1016,
				grnd_level: 1015,
				humidity: 74,
				temp_kf: 0,
			},
			weather: [
				{
					id: 803,
					main: 'Clouds',
					description: 'broken clouds',
					icon: '04d',
				},
			],
			clouds: {
				all: 59,
			},
			wind: {
				speed: 3.67,
				deg: 302,
				gust: 7.52,
			},
			visibility: 10000,
			pop: 0.21,
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-11 12:00:00',
		},
		{
			dt: 1639234800,
			main: {
				temp: 5.98,
				feels_like: 3.55,
				temp_min: 5.98,
				temp_max: 5.98,
				pressure: 1018,
				sea_level: 1018,
				grnd_level: 1017,
				humidity: 77,
				temp_kf: 0,
			},
			weather: [
				{
					id: 800,
					main: 'Clear',
					description: 'clear sky',
					icon: '01d',
				},
			],
			clouds: {
				all: 7,
			},
			wind: {
				speed: 3.19,
				deg: 289,
				gust: 6.73,
			},
			visibility: 10000,
			pop: 0.07,
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-11 15:00:00',
		},
		{
			dt: 1639245600,
			main: {
				temp: 4.75,
				feels_like: 2.51,
				temp_min: 4.75,
				temp_max: 4.75,
				pressure: 1020,
				sea_level: 1020,
				grnd_level: 1019,
				humidity: 80,
				temp_kf: 0,
			},
			weather: [
				{
					id: 802,
					main: 'Clouds',
					description: 'scattered clouds',
					icon: '03n',
				},
			],
			clouds: {
				all: 45,
			},
			wind: {
				speed: 2.61,
				deg: 264,
				gust: 3.73,
			},
			visibility: 10000,
			pop: 0.05,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-11 18:00:00',
		},
		{
			dt: 1639256400,
			main: {
				temp: 4.44,
				feels_like: 1.92,
				temp_min: 4.44,
				temp_max: 4.44,
				pressure: 1021,
				sea_level: 1021,
				grnd_level: 1020,
				humidity: 82,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 98,
			},
			wind: {
				speed: 2.89,
				deg: 226,
				gust: 6.52,
			},
			visibility: 10000,
			pop: 0,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-11 21:00:00',
		},
		{
			dt: 1639267200,
			main: {
				temp: 4.62,
				feels_like: 1.36,
				temp_min: 4.62,
				temp_max: 4.62,
				pressure: 1022,
				sea_level: 1022,
				grnd_level: 1020,
				humidity: 83,
				temp_kf: 0,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
			clouds: {
				all: 99,
			},
			wind: {
				speed: 4.06,
				deg: 204,
				gust: 9.77,
			},
			visibility: 10000,
			pop: 0,
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-12 00:00:00',
		},
		{
			dt: 1639278000,
			main: {
				temp: 4.73,
				feels_like: 1.12,
				temp_min: 4.73,
				temp_max: 4.73,
				pressure: 1021,
				sea_level: 1021,
				grnd_level: 1020,
				humidity: 88,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 4.75,
				deg: 197,
				gust: 10.85,
			},
			visibility: 10000,
			pop: 0.27,
			rain: {
				'3h': 0.1,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-12 03:00:00',
		},
		{
			dt: 1639288800,
			main: {
				temp: 5.15,
				feels_like: 1.05,
				temp_min: 5.15,
				temp_max: 5.15,
				pressure: 1020,
				sea_level: 1020,
				grnd_level: 1019,
				humidity: 90,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 6.09,
				deg: 194,
				gust: 14.26,
			},
			visibility: 10000,
			pop: 0.6,
			rain: {
				'3h': 0.34,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-12 06:00:00',
		},
		{
			dt: 1639299600,
			main: {
				temp: 5.72,
				feels_like: 1.91,
				temp_min: 5.72,
				temp_max: 5.72,
				pressure: 1020,
				sea_level: 1020,
				grnd_level: 1019,
				humidity: 93,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10d',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 5.75,
				deg: 206,
				gust: 13.87,
			},
			visibility: 10000,
			pop: 0.88,
			rain: {
				'3h': 0.57,
			},
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-12 09:00:00',
		},
		{
			dt: 1639310400,
			main: {
				temp: 7.51,
				feels_like: 4.78,
				temp_min: 7.51,
				temp_max: 7.51,
				pressure: 1020,
				sea_level: 1020,
				grnd_level: 1019,
				humidity: 98,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10d',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 4.33,
				deg: 222,
				gust: 11.49,
			},
			visibility: 8293,
			pop: 0.88,
			rain: {
				'3h': 0.39,
			},
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-12 12:00:00',
		},
		{
			dt: 1639321200,
			main: {
				temp: 8.45,
				feels_like: 6.87,
				temp_min: 8.45,
				temp_max: 8.45,
				pressure: 1021,
				sea_level: 1021,
				grnd_level: 1020,
				humidity: 99,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10d',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 2.66,
				deg: 225,
				gust: 6.37,
			},
			visibility: 2383,
			pop: 0.57,
			rain: {
				'3h': 0.82,
			},
			sys: {
				pod: 'd',
			},
			dt_txt: '2021-12-12 15:00:00',
		},
		{
			dt: 1639332000,
			main: {
				temp: 8.48,
				feels_like: 6.72,
				temp_min: 8.48,
				temp_max: 8.48,
				pressure: 1022,
				sea_level: 1022,
				grnd_level: 1020,
				humidity: 99,
				temp_kf: 0,
			},
			weather: [
				{
					id: 500,
					main: 'Rain',
					description: 'light rain',
					icon: '10n',
				},
			],
			clouds: {
				all: 100,
			},
			wind: {
				speed: 2.95,
				deg: 198,
				gust: 7.11,
			},
			visibility: 280,
			pop: 0.59,
			rain: {
				'3h': 0.69,
			},
			sys: {
				pod: 'n',
			},
			dt_txt: '2021-12-12 18:00:00',
		},
	],
	city: {
		id: 2759794,
		name: 'Amsterdam',
		coord: {
			lat: 52.374,
			lon: 4.8897,
		},
		country: 'NL',
		population: 0,
		timezone: 3600,
		sunrise: 1638862566,
		sunset: 1638890886,
	},
};
