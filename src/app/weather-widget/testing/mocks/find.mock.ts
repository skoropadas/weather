import {CitySearch} from '../../common/city-search';

export const FIND_MOCK_DATA: CitySearch = {
	message: 'accurate',
	cod: '200',
	count: 5,
	list: [
		{
			id: 5368361,
			name: 'Los Angeles',
			coord: {
				lat: 34.0522,
				lon: -118.2437,
			},
			main: {
				temp: 288.78,
				feels_like: 288.46,
				temp_min: 286.18,
				temp_max: 291.32,
				pressure: 1016,
				humidity: 79,
			},
			dt: 1638904177,
			wind: {
				speed: 0,
				deg: 0,
			},
			sys: {
				country: 'US',
			},
			rain: null,
			snow: null,
			clouds: {
				all: 90,
			},
			weather: [
				{
					id: 721,
					main: 'Haze',
					description: 'haze',
					icon: '50d',
				},
			],
		},
		{
			id: 3882428,
			name: 'Los Ángeles',
			coord: {
				lat: -37.4667,
				lon: -72.35,
			},
			main: {
				temp: 295.07,
				feels_like: 294.52,
				temp_min: 295.07,
				temp_max: 295.07,
				pressure: 1018,
				humidity: 46,
			},
			dt: 1638904149,
			wind: {
				speed: 3.6,
				deg: 350,
			},
			sys: {
				country: 'CL',
			},
			rain: null,
			snow: null,
			clouds: {
				all: 75,
			},
			weather: [
				{
					id: 803,
					main: 'Clouds',
					description: 'broken clouds',
					icon: '04d',
				},
			],
		},
		{
			id: 1705545,
			name: 'Los Angeles',
			coord: {
				lat: 9.0125,
				lon: 125.6081,
			},
			main: {
				temp: 296.63,
				feels_like: 297.49,
				temp_min: 296.63,
				temp_max: 296.63,
				pressure: 1009,
				humidity: 94,
				sea_level: 1009,
				grnd_level: 1008,
			},
			dt: 1638904182,
			wind: {
				speed: 1.25,
				deg: 342,
			},
			sys: {
				country: 'PH',
			},
			rain: null,
			snow: null,
			clouds: {
				all: 98,
			},
			weather: [
				{
					id: 804,
					main: 'Clouds',
					description: 'overcast clouds',
					icon: '04n',
				},
			],
		},
		{
			id: 2514856,
			name: 'Los Ángeles',
			coord: {
				lat: 28.4667,
				lon: -16.4167,
			},
			main: {
				temp: 287.6,
				feels_like: 287.09,
				temp_min: 285.45,
				temp_max: 291.58,
				pressure: 1026,
				humidity: 76,
			},
			dt: 1638904246,
			wind: {
				speed: 0.45,
				deg: 250,
			},
			sys: {
				country: 'ES',
			},
			rain: null,
			snow: null,
			clouds: {
				all: 75,
			},
			weather: [
				{
					id: 701,
					main: 'Mist',
					description: 'mist',
					icon: '50n',
				},
			],
		},
		{
			id: 3549480,
			name: 'Los Ángeles',
			coord: {
				lat: 20.7869,
				lon: -76.3408,
			},
			main: {
				temp: 305.29,
				feels_like: 308.02,
				temp_min: 305.29,
				temp_max: 305.29,
				pressure: 1015,
				humidity: 51,
			},
			dt: 1638904211,
			wind: {
				speed: 3.09,
				deg: 60,
			},
			sys: {
				country: 'CU',
			},
			rain: null,
			snow: null,
			clouds: {
				all: 40,
			},
			weather: [
				{
					id: 802,
					main: 'Clouds',
					description: 'scattered clouds',
					icon: '03d',
				},
			],
		},
	],
};
