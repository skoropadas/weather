import {Pipe, PipeTransform} from '@angular/core';
import {WeatherUnit} from './common/weather-unit';

@Pipe({
	name: 'speedUnit',
})
export class SpeedUnitPipe implements PipeTransform {
	public transform(value: number, unit: WeatherUnit): string {
		return `${value.toFixed(1)} ${this.unitName(unit)}`;
	}

	private unitName(unit: WeatherUnit): string {
		switch (unit) {
			case WeatherUnit.IMPERIAL:
				return 'm/h';
			default:
				return 'm/s';
		}
	}
}
