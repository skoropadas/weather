import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

/* Simple component that loads the flag icon from the open weather API */
@Component({
	selector: 'i[nz-icon][flag-icon]',
	template: `
		<svg>
			<image [attr.href]="href" />
		</svg>
	`,
	styleUrls: ['./flag-icon.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FlagIconComponent {
	@Input('flag-icon')
	public country: string = '';

	public get href(): string {
		return `https://openweathermap.org/images/flags/${this.country.toLowerCase()}.png`;
	}
}
