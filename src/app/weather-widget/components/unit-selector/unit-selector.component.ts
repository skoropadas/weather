import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {WeatherUnit} from '../../common/weather-unit';

/* The component that helps to choose the unit, I decided not to implement the ControlValueAccessor
, because it would be superfluous in this case */
@Component({
	selector: 'unit-selector',
	templateUrl: './unit-selector.component.html',
	styleUrls: ['./unit-selector.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnitSelectorComponent {
	@Input()
	public unit: WeatherUnit = WeatherUnit.METRIC;

	@Output()
	public unitChange: EventEmitter<WeatherUnit> = new EventEmitter<WeatherUnit>();

	public units: typeof WeatherUnit = WeatherUnit;

	public updateUnit(unit: WeatherUnit): void {
		this.unit = unit;
		this.unitChange.next(this.unit);
	}
}
