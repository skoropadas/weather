import {ComponentHarness, HarnessPredicate, TestElement} from '@angular/cdk/testing';
import {ForecastHarness} from '../../forecast/testing/forecast.harness';
import {CityWeatherHarnessFilter} from './city-weather.harness.filter';

export class CityWeatherHarness extends ComponentHarness {
	public static hostSelector: string = 'city-weather';

	public static with(options: CityWeatherHarnessFilter): HarnessPredicate<CityWeatherHarness> {
		return new HarnessPredicate<CityWeatherHarness>(CityWeatherHarness, options).addOption(
			'trigger name',
			options.name,
			(harness: CityWeatherHarness, name: string) => HarnessPredicate.stringMatches(harness.getName(), name),
		);
	}

	public async getForecast(): Promise<ForecastHarness | null> {
		return await this.locatorForOptional(ForecastHarness)();
	}

	public async click(): Promise<void> {
		return (await this.getWeatherElement()).click();
	}

	public async getName(): Promise<string> {
		return (await this.locatorFor('.city .name')()).text();
	}

	public async clickByDeleteButton(): Promise<void> {
		return (await this.getDeleteButton()).click();
	}

	private async getWeatherElement(): Promise<TestElement> {
		return await this.locatorFor('.weather')();
	}

	private async getDeleteButton(): Promise<TestElement> {
		return await this.locatorFor('.delete-button')();
	}
}
