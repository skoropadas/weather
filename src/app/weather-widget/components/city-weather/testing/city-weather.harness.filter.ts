import {BaseHarnessFilters} from '@angular/cdk/testing';

export interface CityWeatherHarnessFilter extends BaseHarnessFilters {
	name?: string;
}
