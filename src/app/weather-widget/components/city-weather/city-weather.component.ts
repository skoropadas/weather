import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {CityWeather} from '../../common/city-weather';
import {catchError, map, NEVER, Observable, of, shareReplay, startWith, Subject, switchMap} from 'rxjs';
import {WeatherService} from '../../weather.service';
import {ObservableError, ObservablePending, ObservableResult, ObservableState} from '../../common/observable-state';
import {Forecast} from '../../common/forecast';
import {WeatherUnit} from '../../common/weather-unit';
import {animate, style, transition, trigger} from '@angular/animations';
import {preventInitialChildAnimations} from '../../common/prevent-initial-animation';

@Component({
	animations: [
		trigger('inOutAnimation', [
			transition(':enter', [
				style({height: 0, opacity: 0}),
				animate('.2s ease-out', style({height: '*', opacity: 1})),
			]),
			transition(':leave', [
				style({height: '*', opacity: 1}),
				animate('.2s ease-in', style({height: 0, opacity: 0})),
			]),
		]),
		preventInitialChildAnimations,
	],
	selector: 'city-weather',
	templateUrl: './city-weather.component.html',
	styleUrls: ['./city-weather.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CityWeatherComponent implements OnChanges {
	@Input()
	public cityId: number | null = null;

	@Input()
	public opened: boolean = false;

	@Input()
	public unit: WeatherUnit = WeatherUnit.STANDARD;

	@Output()
	public remove: EventEmitter<void> = new EventEmitter<void>();

	@Output()
	public openedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

	public reloadWeather$: Subject<void> = new Subject<void>();
	public weather$: Observable<ObservableState<CityWeather, Error>> = NEVER;

	public reloadForecast$: Subject<void> = new Subject<void>();
	public forecast$: Observable<ObservableState<Forecast, Error>> = NEVER;

	constructor(private weatherService: WeatherService) {}

	public ngOnChanges({cityId, unit}: SimpleChanges): void {
		/* Checking this.cityId instead of cityId.currentValue, just because SimpleChange has no typing */
		if ((cityId || unit) && this.cityId) {
			/* Load weather data, also catching errors that may occur */
			this.weather$ = of(this.cityId).pipe(
				switchMap((cId: number) =>
					this.reloadWeather$.pipe(
						startWith(null),
						switchMap(() =>
							this.weatherService.weather(cId, this.unit).pipe(
								map((result: CityWeather) => new ObservableResult(result)),
								catchError((error: Error) => of(new ObservableError(error))),
								startWith(new ObservablePending()),
							),
						),
					),
				),
			);

			/* Load forecast data, also catching errors that may occur, and caching last result */
			this.forecast$ = of(this.cityId).pipe(
				switchMap((cId: number) =>
					this.reloadForecast$.pipe(
						startWith(null),
						switchMap(() =>
							this.weatherService.forecast(cId, this.unit).pipe(
								map((result: Forecast) => new ObservableResult(result)),
								catchError((error: Error) => of(new ObservableError(error))),
								startWith(new ObservablePending()),
							),
						),
					),
				),
				shareReplay(1),
			);
		}
	}

	public toggleForecast(): void {
		this.opened = !this.opened;
		this.openedChange.next(this.opened);
	}
}
