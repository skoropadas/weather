import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ForecastWeather} from '../../common/forecast-weather';
import {WeatherUnit} from '../../common/weather-unit';

@Component({
	selector: 'forecast',
	templateUrl: './forecast.component.html',
	styleUrls: ['./forecast.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForecastComponent implements OnChanges {
	@Input()
	public forecast: ForecastWeather[] = [];

	@Input()
	public unit: WeatherUnit = WeatherUnit.STANDARD;

	public forecastByDay: Map<number, ForecastWeather[]> = new Map<number, ForecastWeather[]>();

	public ngOnChanges({forecast}: SimpleChanges): void {
		if (forecast) {
			/* Group weather by day to display them in a grouped view */
			this.forecastByDay = new Map<number, ForecastWeather[]>();

			this.forecast.forEach((weather: ForecastWeather) => {
				/* Creating new date and setting zero time, to group them by day */
				const date: number = new Date(this.toMillis(weather.dt)).setUTCHours(0, 0, 0, 0);

				this.forecastByDay.set(date, [...(this.forecastByDay.get(date) ?? []), weather]);
			});
		}
	}

	public toMillis(dateInSeconds: number): number {
		/*
			The date from the backend comes in seconds,
			we have to convert it to milliseconds because the date object supports only this option
		 */
		return dateInSeconds * 1000;
	}
}
