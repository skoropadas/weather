import {BaseHarnessFilters, ComponentHarness, HarnessPredicate} from '@angular/cdk/testing';

export class ForecastHarness extends ComponentHarness {
	public static hostSelector: string = 'forecast';

	public static with(options: BaseHarnessFilters): HarnessPredicate<ForecastHarness> {
		return new HarnessPredicate<ForecastHarness>(ForecastHarness, options);
	}
}
