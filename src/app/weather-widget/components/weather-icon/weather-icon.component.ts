import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

/* Component that loads weather icon that could be animated */
@Component({
	selector: 'weather-icon',
	template: ` <img [attr.alt]="alt" [src]="href" [style.width.px]="size" [style.height.px]="size" /> `,
	styleUrls: ['./weather-icon.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeatherIconComponent {
	@Input()
	public icon: string = '';

	@Input()
	public size: 64 | 48 = 48;

	@Input()
	public animated: boolean = false;

	@Input()
	public alt: string = '';

	public get href(): string {
		return `assets/icons/${this.animated ? 'animated' : 'static'}/${this.icon}.svg`;
	}
}
