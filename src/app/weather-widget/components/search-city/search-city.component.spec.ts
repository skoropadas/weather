import {HarnessLoader, TestElement} from '@angular/cdk/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
import {Component} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {SearchCityHarness} from './testing/search-city.harness';
import {provideWeatherMockService} from '../../testing/weather.mock.service';
import {WeatherWidgetModule} from '../../weather-widget.module';
import {FIND_MOCK_DATA} from '../../testing/mocks/find.mock';

describe('SearchCityComponent', () => {
	let fixture: ComponentFixture<TestingComponent>;
	let loader: HarnessLoader;
	let component: SearchCityHarness;
	let testComponent: TestingComponent;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [WeatherWidgetModule, NoopAnimationsModule, HttpClientModule],
			providers: [provideWeatherMockService()],
			declarations: [TestingComponent],
		}).compileComponents();
		fixture = TestBed.createComponent(TestingComponent);
		testComponent = fixture.componentInstance;
		loader = TestbedHarnessEnvironment.loader(fixture);
		component = await loader.getHarness(SearchCityHarness);
	});

	it('should return cityId', async () => {
		await component.enterText(FIND_MOCK_DATA.list[0].name);

		const option: TestElement | null = await component.getOption(FIND_MOCK_DATA.list[0].name);

		await option?.click();

		expect(testComponent.cityId).toBe(FIND_MOCK_DATA.list[0].id);
	});
});

@Component({
	selector: 'testing-component',
	template: ` <search-city (selectCity)="cityId = $event"></search-city> `,
})
export class TestingComponent {
	public cityId: number = 0;
}
