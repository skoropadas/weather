import {BaseHarnessFilters, ComponentHarness, HarnessPredicate, TestElement} from '@angular/cdk/testing';

export class SearchCityHarness extends ComponentHarness {
	public static hostSelector: string = 'search-city';

	public static with(options: BaseHarnessFilters): HarnessPredicate<SearchCityHarness> {
		return new HarnessPredicate<SearchCityHarness>(SearchCityHarness, options);
	}

	public async enterText(text: string): Promise<void> {
		return (await this.getInputElement()).sendKeys(text);
	}

	public async getOption(text: string): Promise<TestElement | null> {
		const options: TestElement[] = await this.documentRootLocatorFactory().locatorForAll('nz-option-item .name')();

		for (const option of options) {
			if (await HarnessPredicate.stringMatches(option.text(), text)) {
				return option
			}
		}

		return null;
	}

	private async getInputElement(): Promise<TestElement> {
		return await this.locatorFor('input')();
	}
}
