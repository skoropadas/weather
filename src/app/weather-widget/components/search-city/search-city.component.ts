import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';
import {
	BehaviorSubject,
	catchError,
	debounceTime,
	distinctUntilChanged,
	filter,
	map,
	Observable,
	of,
	startWith,
	switchMap,
} from 'rxjs';
import {CitySearch} from '../../common/city-search';
import {WeatherService} from '../../weather.service';
import {CityWeather} from '../../common/city-weather';
import {ObservableError, ObservablePending, ObservableResult, ObservableState} from '../../common/observable-state';

@Component({
	selector: 'search-city',
	templateUrl: './search-city.component.html',
	styleUrls: ['./search-city.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchCityComponent {
	@Output()
	public selectCity: EventEmitter<number> = new EventEmitter<number>();

	public selected: null = null;
	public search$: BehaviorSubject<string> = new BehaviorSubject<string>('');
	public items$: Observable<ObservableState<CityWeather[], Error>>;

	constructor(private weatherService: WeatherService) {
		/* The simple value search query that waits 300 milliseconds after input, and discards duplicate values */
		this.items$ = this.search$.pipe(
			debounceTime(300),
			distinctUntilChanged(),
			filter((searchStr: string) => !!searchStr),
			switchMap((searchStr: string) =>
				this.weatherService.find(searchStr).pipe(
					map((result: CitySearch) => new ObservableResult(result.list)),
					catchError((error: Error) => of(new ObservableError(error))),
					startWith(new ObservablePending()),
				),
			),
		);
	}
}
