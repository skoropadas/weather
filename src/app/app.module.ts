import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {WeatherWidgetModule} from './weather-widget/weather-widget.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../environments/environment';

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		WeatherWidgetModule.forRoot({
			appId: environment.appId,
			apiUrl: environment.apiUrl,
		}),
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
