# Weather

## How to run

Run:
```
npm i && ng s
```

## How to test

Run:
```
ng test
```

## What's done:

- The app shows five cities by default (Amsterdam, Moscow, London, Tallinn, New York)
- For each city, information about its name, current temperature, feels like temperature, wind speed and current weather is displayed
- If you click on any of the cities, you can see detailed weather information for every 3 hours
- Wrote tests for the main functionality like searching and selecting a city, adding a city, deleting a city and expand the forecast. The tests are written using the ComponentHarness interfaces from `@angular/cdk` library

## Additional functionality:

- The weather widget is developed as a module
- Cities can be added and removed (the current list is saved)
- You can find and select a city by its full name in the field above, after which it will be added to the list
- If you hover the cursor over a city, the button to delete city will appear on its right side
- At the top of the app, you can select the units
- The weather forecast is cached and not re-requested after reopening
